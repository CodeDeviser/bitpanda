<?php

namespace App\Interfaces;

interface TransactionInterface
{
    public function getData();
}