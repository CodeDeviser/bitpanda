<?php

namespace App\Providers;

use App\Services\DBService;
use App\Services\CSVService;
use Illuminate\Support\ServiceProvider;
use App\Interfaces\TransactionInterface;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(TransactionInterface::class, DBService::class);
        $this->app->bind(TransactionInterface::class, CSVService::class);
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
