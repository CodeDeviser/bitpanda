<?php

namespace App\Services;

use App\Interfaces\TransactionInterface;
use App\Models\Transaction;

class DBService implements TransactionInterface {

    /**
     * Get data from Transactions Table
     * 
     * @return Collection
     */
    public function getData()
    {
        return Transaction::all();
    }
}