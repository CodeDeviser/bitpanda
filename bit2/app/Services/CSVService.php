<?php

namespace App\Services;

use App\Interfaces\TransactionInterface;

class CSVService implements TransactionInterface {

    /**
     * Get Data from CSV file
     * 
     * @return Array
     */
    public function getData()
    {
        $file = base_path('transactions.csv');
        
        return $this->csvToArray($file);
    }

    /**
     * Converts CSV file to Array
     * 
     * @return Array
     */
    public function csvToArray($filename = '', $delimiter = ',')
    {
        if (!file_exists($filename) || !is_readable($filename))
            return false;

        $header = null;
        $data = array();
        if (($handle = fopen($filename, 'r')) !== false)
        {
            while (($row = fgetcsv($handle, 1000, $delimiter)) !== false)
            {
                if (!$header)
                    $header = $row;
                else
                    $data[] = array_combine($header, $row);
            }
            fclose($handle);
        }

        return $data;
    }
}