<?php

namespace App\Services;

use App\Interfaces\TransactionInterface;
use App\Services\DBService;
use App\Services\CSVService;
use App\Services\ResponseService;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class TransactionService extends ResponseService {

    /**
     * Check source and get data from service
     * 
     * @return \Illuminate\Http\Response|\Illuminate\Contracts\Routing\ResponseFactory 
     */
    public function getData($source)
    {
        try{
            if($source != 'db' && $source != 'csv')
            {
                throw new ModelNotFoundException('Source ' . $source . ' is unknown. ');
            }
        }
        catch(ModelNotFoundException $exception)
        {
            return $exception->getMessage();
        }
      
        $source == 'db' ? $service = new DBService() : $service = new CSVService();
        return $this->sendResponse($service->getData(), 'Data retrieved successfully.');     
    }

}