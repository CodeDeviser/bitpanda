<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Services\TransactionService;

class TransactionController extends BaseController
{
    protected $transactionService;

    /**
     * Constructor 
     * 
     * @param App\Services\TransactionService
     */
    public function __construct(TransactionService $transactionService)
    {
        $this->transactionService = $transactionService;
    }

    /**
     * Display a listing of the resource.
     * 
     * @return \Illuminate\Http\Response
     */
    public function getServiceData(Request $request)
    {
        return $this->transactionService->getData($request->Input('source'));
    }
}
