<?php


namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller as Controller;
use App\Services\ResponseService;

class BaseController extends Controller
{

    protected $responseService;

    /**
     * Constructor
     * 
     * @param App\Services\ResponseService
     */
    public function __construct(ResponseService $responseService)
    {
        $this->responseService = $responseService;
    }

    /**
     * success response method.
     *
     * @return \Illuminate\Http\Response
     */
    public function sendResponse($result, $message)
    {
    	$this->responseService->sendResponse($result, $message);
    }


    /**
     * return error response.
     *
     * @return \Illuminate\Http\Response
     */
    public function sendError($error, $errorMessages = [], $code = 404)
    {
    	$this->responseService($error, $errorMessages, $code);
    }
}