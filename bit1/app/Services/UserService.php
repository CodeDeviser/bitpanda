<?php

namespace App\Services;

use Validator;
use App\Services\ResponseService;
use App\Models\UserDetail;
use App\Models\User;
use App\Http\Resources\UserResource;

class UserService extends ResponseService {

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\User $user
     * @return \Illuminate\Http\Response
     */
    public function update($request, $user)
    {
        $input = $request->all();
   
        $validator = Validator::make($input, [
            'first_name' => 'required',
            'last_name' => 'required',
            'phone_number' => 'required'
        ]);
   
        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());       
        }
        
        $userDetails = UserDetail::where('user_id', $user->id)->first();
        if($userDetails === null){
            return $this->sendResponse([], 'User Details doesn´t exist.');
        }

        $userDetails->first_name = $input['first_name'];
        $userDetails->last_name = $input['last_name'];
        $userDetails->phone_number = $input['phone_number'];
        $userDetails->save();
   
        return $this->sendResponse($userDetails, 'User Details updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\User $user
     * @return \Illuminate\Http\Response
     */
    public function destroy($user)
    {
        $userDetails = UserDetail::where('user_id', $user->id)->first();
        if($userDetails === null){
            $user->delete();
            return $this->sendResponse([$user], 'User deleted successfully.');
        }
   
        return $this->sendResponse([], 'User have details already, can´t be deleted.');
    }

    /**
     * Get user by active and citizenship
     * 
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function getByActiveAndCitizenship($citizenship_id)
    {
        $users = User::whereHas('details', function ($details) use ($citizenship_id) {
            $details->where('citizenship_country_id', $citizenship_id);
        })->where('active', true)->get();

        return $this->sendResponse(UserResource::collection($users), 'Users retrieved successfully.');
    }
}