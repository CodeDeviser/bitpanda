<?php

namespace App\Models;

use Illuminate\Foundation\Auth\User as Authenticatable;

class UserDetail extends Authenticatable
{

    /**
     * Set timestamps to false and override the default setting
     */
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'id',
        'user_id',
        'citizenship_country_id',
        'first_name',
        'last_name',
        'phone_number',
    ];

    /**
     *  Belongs to user
     * 
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(user::class, 'user_id');
    }
}
