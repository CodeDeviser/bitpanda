<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\TestCase;
use Symfony\Component\HttpFoundation\Response;
use App\Models\User;

class DeleteUserTest extends TestCase
{
    use DatabaseMigrations;
    use WithFaker;
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_delete_a_user_with_valid_data()
    {
        
        $user = User::factory()->create();
        
        $response = $this->json('delete', "/api/users/$user->id");
   
        $response->assertStatus(Response::HTTP_OK)
            ->assertValid([
                $user->email => 'Valid email address',
                $user->active => 'Valid active format'
            ]);
       
    }

    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_delete_a_user_with_invalid_data()
    {
        $user = User::create([
            'email' => 'teste.com',
            'active' => 'foo'
        ]);
        
        $response = $this->json('delete', "/api/users/$user->id");
   
        $response->assertStatus(Response::HTTP_OK)
            ->assertInvalid([
                $user->email => 'Invalid email address',
                $user->active => 'Invalid active format'
            ]);
    }
}
