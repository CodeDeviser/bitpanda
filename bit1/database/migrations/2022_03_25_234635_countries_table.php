<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('countries', function (Blueprint $table) {
            $table->integer('id')->autoIncrement();
            $table->string('name', 63)->collation('utf8mb4_unicode_ci')->NotNull()->Comment('English country name.');
            $table->char('iso2', 2)->collation('ascii_bin')->NotNull()->Comment('ISO 3266-2 two letter upper case country code.');
            $table->char('iso3', 3)->collation('ascii_bin')->NotNull()->Comment('ISO 3166-3 three letter upper case count code.');                      
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('countries');
    }
};
