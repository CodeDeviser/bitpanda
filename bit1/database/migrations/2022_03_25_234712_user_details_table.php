<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_details', function (Blueprint $table) {
            $table->integer('id')->autoIncrement();
            $table->integer('user_id')->NotNull();
            $table->integer('citizenship_country_id')->NotNull();
            $table->string('first_name')->NotNull();
            $table->string('last_name')->NotNull();
            $table->string('phone_number')->NotNull();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('user_details');
    }
};
